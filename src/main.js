import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'
// import Vuelidate from 'vuelidate'
// import * as VeeValidate from "vee-validate";
import axios from 'axios'
import VueAxios from 'vue-axios'
import en from 'vee-validate/dist/locale/en.json';
import * as rules from 'vee-validate/dist/rules';
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fullcalendar/bootstrap/main.css'
import '@fullcalendar/core/main.css'
import '@fullcalendar/daygrid/main.css'
import '@fullcalendar/timegrid/main.css'
import 'vue-datetime/dist/vue-datetime.css'





import App from './components/HelloWorld.vue'

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

localize('en', en);


// Vue.use(VeeValidate);

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

Vue.config.productionTip = false
// Vue.use(Vuelidate);
Vue.use(BootstrapVue,VueAxios, axios)



new Vue({
  render: h => h(App),
}).$mount('#app')
